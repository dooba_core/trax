/* Dooba SDK
 * Library for the Trax vehicle platform
 */

// External Includes
#include <stdlib.h>
#include <pwm/pwm.h>

// Internal Includes
#include "trax.h"

// Pre-Initialize Trax
void trax_preinit()
{
	// Enable WiFi
	dio_output(TRAX_WIFI_EN);
	dio_hi(TRAX_WIFI_EN);

	// Configure Kraft I/O
	pwm_set(TRAX_KA_PWM, 0);
	dio_output(TRAX_KA_FWD);
	dio_output(TRAX_KA_REV);
	dio_lo(TRAX_KA_FWD);
	dio_lo(TRAX_KA_REV);
	pwm_set(TRAX_KB_PWM, 0);
	dio_output(TRAX_KB_FWD);
	dio_output(TRAX_KB_REV);
	dio_lo(TRAX_KB_FWD);
	dio_lo(TRAX_KB_REV);

	// Configure LEDs
	dio_output(TRAX_LED_A);
	dio_output(TRAX_LED_B);
	trax_led_a_off();
	trax_led_b_off();
}

// Set Kraft Channels Speed and Direction (-128 to 128)
void trax_set_k(int a, int b)
{
	// Handle Track Side Inversion
	#ifdef TRAX_INVERT_SIDES
		int x;
		x = a;
		a = b;
		b = x;
	#endif

	// Set Channels
	trax_set_ka(a);
	trax_set_kb(b);
}

// Set Kraft Channel A Speed and Direction (-128 to 128)
void trax_set_ka(int v)
{
	uint16_t x;

	// Handle Track Direction Inversion
	#ifdef TRAX_INVERT_DIR
		v = -v;
	#endif

	// Set Value
	x = abs(v);
	x = x * 2;
	if(x > 255)															{ x = 255; }
	pwm_set(TRAX_KA_PWM, x);

	// Set Direction
	if(v > 0)															{ dio_hi(TRAX_KA_FWD); dio_lo(TRAX_KA_REV); }
	else if(v < 0)														{ dio_hi(TRAX_KA_REV); dio_lo(TRAX_KA_FWD); }
	else																{ dio_lo(TRAX_KA_FWD); dio_lo(TRAX_KA_REV); }
}

// Set Kraft Channel B Speed and Direction (-128 to 128)
void trax_set_kb(int v)
{
	uint16_t x;

	// Handle Track Direction Inversion
	#ifdef TRAX_INVERT_DIR
		v = -v;
	#endif

	// Set Value
	x = abs(v);
	x = x * 2;
	if(x > 255)															{ x = 255; }
	pwm_set(TRAX_KB_PWM, x);

	// Set Direction
	if(v > 0)															{ dio_hi(TRAX_KB_FWD); dio_lo(TRAX_KB_REV); }
	else if(v < 0)														{ dio_hi(TRAX_KB_REV); dio_lo(TRAX_KB_FWD); }
	else																{ dio_lo(TRAX_KB_FWD); dio_lo(TRAX_KB_REV); }
}
