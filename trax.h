/* Dooba SDK
 * Library for the Trax vehicle platform
 */

#ifndef	__TRAX_H
#define	__TRAX_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <dio/dio.h>

// WiFi Enable Pin
#define	TRAX_WIFI_EN								8

// LED Pins
#define	TRAX_LED_A									24
#define	TRAX_LED_B									25

// Kraft Control
#define	TRAX_KA_PWM									18
#define	TRAX_KA_FWD									28
#define	TRAX_KA_REV									29
#define	TRAX_KB_PWM									19
#define	TRAX_KB_FWD									27
#define	TRAX_KB_REV									26

// LED Shortcuts
#define	trax_led_a_on()								dio_hi(TRAX_LED_A);
#define	trax_led_a_off()							dio_lo(TRAX_LED_A);
#define	trax_led_b_on()								dio_hi(TRAX_LED_B);
#define	trax_led_b_off()							dio_lo(TRAX_LED_B);

// Pre-Initialize Trax
extern void trax_preinit();

// Set Kraft Channels Speed and Direction (-128 to 128)
extern void trax_set_k(int a, int b);

// Set Kraft Channel A Speed and Direction (-128 to 128)
extern void trax_set_ka(int v);

// Set Kraft Channel B Speed and Direction (-128 to 128)
extern void trax_set_kb(int v);

#endif
